import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk, ImageOps, ImageEnhance, ImageFilter
from ttkthemes import ThemedTk
import webbrowser

# Default dimensions
GUI_WIDTH = 1280
GUI_HEIGHT = 768
PADDING = 20
DEFAULT_IMAGE_PATH = "default.jpg"
CONTAINER_WIDTH = GUI_WIDTH - PADDING * 2
ORIGINAL_IMAGE_WIDTH = int(CONTAINER_WIDTH * 0.4)

# Define global variables
original_image = None
linocut_image = None
threshold_value = 128
brightness_value = 0
contrast_value = 1
blur_value = 0
sharpen_value = 0

class ImageFrame(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        self.frame = tk.Frame(self)
        self.frame.pack(fill=tk.BOTH, expand=True)

def adjust_brightness_contrast(image, brightness=0, contrast=1):
    enhancer = ImageEnhance.Brightness(image)
    image = enhancer.enhance(1 + brightness / 255.0)

    enhancer = ImageEnhance.Contrast(image)
    image = enhancer.enhance(contrast)

    return image

def apply_blur_sharpen(image, blur=0, sharpen=0):
    if blur > 0:
        image = image.filter(ImageFilter.GaussianBlur(blur))

    if sharpen > 0:
        image = image.filter(ImageFilter.UnsharpMask(sharpen))

    return image

def create_linocut(input_image, threshold, brightness, contrast, blur, sharpen):
    if input_image is None:
        return None

    # Adjust brightness and contrast
    adjusted_image = adjust_brightness_contrast(input_image, brightness, contrast)

    # Apply blur/sharpen effect
    blurred_image = apply_blur_sharpen(adjusted_image, blur, sharpen)

    # Convert the image to grayscale
    grayscale_image = blurred_image.convert("L")

    # Apply threshold to create a high-contrast black and white image
    threshold_image = grayscale_image.point(lambda x: 255 if x > threshold else 0, "1")

    # Invert colors to make the linocut design black on a white background
    linocut_image = ImageOps.invert(threshold_image)

    return linocut_image

def choose_image():
    global original_image, linocut_image, original_image_tk, linocut_image_tk, slider, save_button, threshold_label

    # Choose an image file
    file_path = filedialog.askopenfilename()
    if file_path:
        # Open the selected image
        original_image = Image.open(file_path)

        # Create linocut image with the default threshold value
        linocut_image = create_linocut(original_image, threshold_value, brightness_value, contrast_value, blur_value, sharpen_value)

        # Resize images to fit the GUI while maintaining aspect ratio for display
        resized_original_image = original_image.resize((ORIGINAL_IMAGE_WIDTH, int(original_image.height * ORIGINAL_IMAGE_WIDTH / original_image.width)))
        resized_linocut_image = linocut_image.resize((ORIGINAL_IMAGE_WIDTH, int(linocut_image.height * ORIGINAL_IMAGE_WIDTH / linocut_image.width)))

        # Convert images to Tkinter PhotoImage
        original_image_tk = ImageTk.PhotoImage(resized_original_image)
        linocut_image_tk = ImageTk.PhotoImage(resized_linocut_image)

        # Display images on canvas
        original_image_label.config(image=original_image_tk)
        linocut_image_label.config(image=linocut_image_tk)

        # Set slider text color to black
        slider.config(fg="black")
        slider.config(troughcolor="white")

        # Show threshold label, slider, and save button
        threshold_label.pack(fill=tk.X)
        slider.pack(fill=tk.X)
        brightness_slider.pack(fill=tk.X)
        contrast_slider.pack(fill=tk.X)
        blur_slider.pack(fill=tk.X)
        sharpen_slider.pack(fill=tk.X)
        save_button.pack(fill=tk.X)

    else:
        # Hide threshold label, slider, and save button
        threshold_label.pack_forget()
        slider.pack_forget()
        brightness_slider.pack_forget()
        contrast_slider.pack_forget()
        blur_slider.pack_forget()
        sharpen_slider.pack_forget()
        save_button.pack_forget()

def save_image():
    global linocut_image
    save_path = filedialog.asksaveasfilename(defaultextension=".jpg", filetypes=[("JPEG files", "*.jpg")])
    if save_path:
        # Save the linocut image with the same dimensions as the original
        linocut_image.save(save_path, dpi=(300,300))

def update_threshold(new_threshold):
    global linocut_image, linocut_image_tk
    threshold_value = int(new_threshold)
    linocut_image = create_linocut(original_image, threshold_value, brightness_value, contrast_value, blur_value, sharpen_value)
    if linocut_image:
        resized_linocut_image = linocut_image.resize((ORIGINAL_IMAGE_WIDTH, int(linocut_image.height * ORIGINAL_IMAGE_WIDTH / linocut_image.width)))
        linocut_image_tk = ImageTk.PhotoImage(resized_linocut_image)
        linocut_image_label.config(image=linocut_image_tk)

def update_brightness_contrast(new_brightness, new_contrast):
    global linocut_image, linocut_image_tk
    brightness_value = int(new_brightness)
    contrast_value = float(new_contrast)
    linocut_image = create_linocut(original_image, threshold_value, brightness_value, contrast_value, blur_value, sharpen_value)
    if linocut_image:
        resized_linocut_image = linocut_image.resize((ORIGINAL_IMAGE_WIDTH, int(linocut_image.height * ORIGINAL_IMAGE_WIDTH / linocut_image.width)))
        linocut_image_tk = ImageTk.PhotoImage(resized_linocut_image)
        linocut_image_label.config(image=linocut_image_tk)

def update_blur_sharpen(new_blur, new_sharpen):
    global linocut_image, linocut_image_tk
    blur_value = int(new_blur)
    sharpen_value = float(new_sharpen)
    linocut_image = create_linocut(original_image, threshold_value, brightness_value, contrast_value, blur_value, sharpen_value)
    if linocut_image:
        resized_linocut_image = linocut_image.resize((ORIGINAL_IMAGE_WIDTH, int(linocut_image.height * ORIGINAL_IMAGE_WIDTH / linocut_image.width)))
        linocut_image_tk = ImageTk.PhotoImage(resized_linocut_image)
        linocut_image_label.config(image=linocut_image_tk)

# Create themed main window
root = ThemedTk(theme="arc")
root.title("LinoDesign")
root.geometry(f"{GUI_WIDTH}x{GUI_HEIGHT}")

# Create image frame
frame = ImageFrame(root)

# Load default image
default_image = Image.open(DEFAULT_IMAGE_PATH)

# Display original image
original_image_label = tk.Label(frame.frame)
original_image_label.pack(side=tk.LEFT, padx=(PADDING, 0))

# Display linocut image
linocut_image_label = tk.Label(frame.frame)
linocut_image_label.pack(side=tk.RIGHT, padx=(0, PADDING))

# Create a container frame for buttons, slider, and label
container = tk.Frame(frame.frame, padx=10)
container.pack(side=tk.TOP, pady=20)

# Headline
headline_label = tk.Label(container, text="LinoDesign", font=("Helvetica", 20, "bold"))
headline_label.pack(fill=tk.X, pady=(0, 10))

# Choose Image button
choose_button = tk.Button(container, text="Choose Image", command=choose_image, width=int(GUI_WIDTH * 0.2 / 10))
choose_button.pack(pady=(0, 10))

# Threshold label
threshold_label = tk.Label(container, text="Threshold:", fg="black")

# Slider for adjusting threshold
slider = tk.Scale(container, from_=0, to=255, orient=tk.HORIZONTAL, command=update_threshold)

# Create sliders for brightness/contrast adjustment
brightness_slider = tk.Scale(container, from_=-255, to=255, orient=tk.HORIZONTAL, label="Brightness", command=lambda val: update_brightness_contrast(val, contrast_slider.get()))

contrast_slider = tk.Scale(container, from_=0, to=2, resolution=0.01, orient=tk.HORIZONTAL, label="Contrast", command=lambda val: update_brightness_contrast(brightness_slider.get(), val))

# Create sliders for blur/sharpen effect
blur_slider = tk.Scale(container, from_=0, to=20, orient=tk.HORIZONTAL, label="Blur", command=lambda val: update_blur_sharpen(val, sharpen_slider.get()))

sharpen_slider = tk.Scale(container, from_=0, to=5, orient=tk.HORIZONTAL, label="Sharpen", command=lambda val: update_blur_sharpen(blur_slider.get(), val))

# Save Image button
save_button = tk.Button(container, text="Save Image", command=save_image, width=int(GUI_WIDTH * 0.2 / 10))

# Text for "more on gitlab"
text_frame = tk.Frame(container, pady=10)
text_frame.pack(side=tk.TOP)

zookee_link = tk.Label(text_frame, text="more on gitlab", fg="blue", cursor="hand2")
zookee_link.pack(side=tk.LEFT)
zookee_link.bind("<Button-1>", lambda event: webbrowser.open_new("https://gitlab.com/zookee1/lino-design"))

# Center the container vertically
container.place(relx=0.5, rely=0.5, anchor=tk.CENTER)

# Default Image
original_image_tk = ImageTk.PhotoImage(default_image.resize((ORIGINAL_IMAGE_WIDTH, int(default_image.height * ORIGINAL_IMAGE_WIDTH / default_image.width))))
original_image_label.config(image=original_image_tk)

# Default Linocut
linocut_image = create_linocut(default_image, threshold_value, brightness_value, contrast_value, blur_value, sharpen_value)
linocut_image_tk = ImageTk.PhotoImage(linocut_image.resize((ORIGINAL_IMAGE_WIDTH, int(linocut_image.height * ORIGINAL_IMAGE_WIDTH / linocut_image.width))))
linocut_image_label.config(image=linocut_image_tk)

# Start the GUI event loop
root.mainloop()
